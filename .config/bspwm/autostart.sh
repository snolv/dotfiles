#!/bin/bash

#==============================================================#
#=====[ FUNCTIONS
#==============================================================#
function run {
  if ! pgrep $1 ;
  then
    $@&
  fi
}


#==============================================================#
#=====[ LAUNCHERS
#==============================================================#
$HOME/.config/polybar/launch.sh &
run sxhkd -c ~/.config/sxhkd/sxhkdrc &
picom --config $HOME/.config/picom/picom.conf &
run nm-applet &
numlockx on &
blueberry-tray &
run volumeicon &

#==============================================================#
#=====[ CONFIGURATION
#==============================================================#
xsetroot -cursor_name left_ptr &
setxkbmap -layout us 
~/.fehbg &